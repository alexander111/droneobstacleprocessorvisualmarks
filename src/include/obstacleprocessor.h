#pragma once

#include <fstream>
#include <string>
#include <sstream>

#include <iostream>
#include <stdio.h>


#include "landmark3d.h"


#include <vector> 
#include <map>


#include "obstacle.h"
#include "circle.h"
#include "ellipse.h"
#include "rectangle.h"


struct comp_operator
{
  bool operator()(Landmark3D lm1, Landmark3D lm2) const
  {
	 return (lm1.id > lm2.id);   // so that the set is ordered in descending order
  }
};
typedef std::map<int,Landmark3D,comp_operator> Landmark3DMap;


class ObstacleProcessor
{

public:
	ObstacleProcessor(void);
	~ObstacleProcessor(void);
	
    std::vector<Obstacle2D*> calculateObstacles(std::vector<Landmark3D> v);
	
    // JL: Should be protected
protected:
public:
    // List of detected obstacles calculated with the landmarks
    std::vector<Obstacle2D*> detectedObstacles;

    // List of existing poles to be calculated with the landmarks
    std::vector<Circle> existingPoles;

    // List of previously known obstacles
    std::vector<Obstacle2D*> previouslyKnownObstacles;


public:
    void initialization(std::string paramsFileName);

    // Wall and window related stuff -> For IMAV 2013
protected:
	double wall_width;
	double wall_length;
	double large_window_length;
	double small_window_length;
	double known_pole_rad;
	double unknown_pole_rad;

    double aruco_width;
	
	int num_landmarks_wind;
	int windows_distribution_option;
	
};


